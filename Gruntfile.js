module.exports = function (grunt) {
	//var taskArray = ['copy:main', 'concat', 'uglify', 'cssmin', 'copy:toweb', 'watch'];
	var taskArray = ['jshint', 'copy:main', 'concat', 'uglify', 'cssmin', 'copy:toweb', 'watch'];
	var pkgObj = grunt.file.readJSON('package.json');
	grunt.initConfig({
		pkg: pkgObj,
		meta: {
			banner: '/*! <%= pkg.name %>-v<%= pkg.version %> - ' +
				'<%= grunt.template.today("yyyy-mm-dd HH:mi:ss") %> \nCopyright (c): kevin.huang <%= pkg.site %> \nReleased under MIT License*/'
		},
		jshint: {
			options: {
				jshintrc: '.jshintrc'
			},
			build: [				
				'src/javascript/public/basic.js',
				'src/javascript/public/colorpicker.js',
				'src/javascript/public/panel.js',
				'src/javascript/public/plugin.js',
				'src/javascript/public/resize.js',
				'src/javascript/public/toolbar.js',
				'src/javascript/public/upload.js',
				'src/javascript/public/utils.js',
				'src/javascript/widget/accordion.js',
				'src/javascript/widget/combox.js',
				'src/javascript/widget/ctxmenu.js',
				'src/javascript/widget/datagrid.js',
				'src/javascript/widget/labeltab.js',
				'src/javascript/widget/layout.js',
				'src/javascript/widget/mvvm.js',
				'src/javascript/widget/pagination.js',
				'src/javascript/widget/tab.js',
				'src/javascript/widget/tree.js',
				'src/javascript/widget/validate.js',
				'src/javascript/widget/menu.js',
				'src/javascript/widget/navmenu.js',
				'src/javascript/widget/calendar.js'
			]
		},
		copy: {			
			main: {
				files: [				
					{ expand: true, cwd: 'src/javascript/public/', src: ['**'], dest: 'build/javascript/' },
					{ expand: true, cwd: 'src/javascript/widget/', src: ['**'], dest: 'build/javascript/' },
					{ expand: true, cwd: 'src/theme/', src: ['**'], dest: 'build/theme/' },

					{ expand: true, cwd: 'src/javascript/public/', src: ['**'], dest: 'E:/myprojects/bui-flow/src/javascript/bui' },//拷贝至flow
					{ expand: true, cwd: 'src/theme/', src: ['**'], dest: 'E:/myprojects/bui-flow/src/theme' },//拷贝至flow
				
					{ expand: true, cwd: 'src/javascript/public/', src: ['**'], dest: 'E:/myprojects/Bui-Editor/src/javascript/bui' },//拷贝至editor
					{ expand: true, cwd: 'src/theme/', src: ['**'], dest: 'E:/myprojects/Bui-Editor/src/theme' }//拷贝至editor
				]
			},
			toweb: {
				files: [
					//复制至bui-ssm
					{
						src: ['build/javascript/config.js'],
						dest: 'E:/myprojects/bui-ssm/web/src/main/webapp/static/lib/config.js'
					}, 
					{
						src: ['build/<%= pkg.toweb %>/bui-<%= pkg.version %>.js'],
						dest: 'E:/myprojects/bui-ssm/web/src/main/webapp/static/lib/bui-<%= pkg.version %>.js'
					},
					{ expand: true, cwd: 'build/theme/', src: ['**'], dest: 'E:/myprojects/bui-ssm/web/src/main/webapp/static/theme' },

					//复制到 bui项目	
					{ expand: true, cwd: 'build/', src: ['**'], dest: 'E:/myprojects/bui-ssm/bui/src/main/webapp' }
				]
			}
		},
		uglify: {
			options: {
				mangle: true, //不混淆变量名
				stripBanners: true,
				banner: '/*! <%= pkg.name %> - v<%= pkg.version %> - ' +
					'<%= grunt.template.today("yyyy-mm-dd") %> \nCopyright (c): kevin.huang <%= pkg.site %> \nReleased under MIT License*/\n'
			},
			buildall: {//压缩所有js
				files: [{
					expand: true,
					cwd: 'build/javascript',//js目录下
					src: '**/*.js',//所有js文件
					dest: 'build/javascript-min'//输出到此目录下
				}]
			}
		},
		cssmin: {
			options: {
				mergeIntoShorthands: false,
				roundingPrecision: -1
			},
			target: {
				files: {
					'build/theme/font/bui-fonts.min.css': ['build/theme/font/bui-fonts.css'],
					'build/theme/default/main.min.css': ['build/theme/default/main.css'],
					'build/theme/blue/main.min.css': ['build/theme/blue/main.css']
				}
			}
		},
		concat: {
			options: {
				process: function(src, filepath) {
					if(filepath.indexOf("basic") > 0){	
						src = src.replace("_ver_var_", pkgObj.name + "-" +  pkgObj.version);
						src = src.replace("_ver_site_", pkgObj.site);
						src = src.replace("_ver_date_", grunt.template.today("yyyy-mm-dd"));
						console.log("make  version  mssage");
						return src;
					}
					return src;
				},
				separator: '',
				stripBanners: true,
				banner: '/*! <%= pkg.name %> - v<%= pkg.version %> - ' +
					'<%= grunt.template.today("yyyy-mm-dd hh:mm:ss") %> \nCopyright (c): kevin.huang  <%= pkg.site %> \nReleased under MIT License*/\n'
			},			
			/***合并UI库****/
			dist: {
				src: [
					'build/javascript/basic.js',
					'build/javascript/utils.js',
					'build/javascript/accordion.js',
					'build/javascript/colorpicker.js',
					'build/javascript/combox.js',
					'build/javascript/ctxmenu.js',
					'build/javascript/datagrid.js',
					'build/javascript/labeltab.js',
					'build/javascript/layout.js',
					'build/javascript/mvvm.js',
					'build/javascript/pagination.js',
					'build/javascript/panel.js',
					'build/javascript/plugin.js',
					'build/javascript/resize.js',
					'build/javascript/tab.js',
					'build/javascript/toolbar.js',
					'build/javascript/tree.js',
					'build/javascript/upload.js',
					'build/javascript/validate.js',
					'build/javascript/menu.js',
					'build/javascript/navmenu.js',
					'build/javascript/calendar.js'
				],
				dest: 'build/javascript/<%= pkg.name %>-<%= pkg.version %>.js'
			}
		},
		watch: {
			build: {
				files: ['src/javascript/public/*.js', 'src/javascript/widget/*.js', 'src/theme/default/*.css'],
				tasks: taskArray,
				options: { spawn: false }
			}
		}
	});
	grunt.loadNpmTasks('grunt-contrib-copy');
	grunt.loadNpmTasks('grunt-contrib-concat');
	grunt.loadNpmTasks('grunt-contrib-watch');
	grunt.loadNpmTasks('grunt-contrib-jshint');
	grunt.loadNpmTasks('grunt-contrib-cssmin');
	grunt.loadNpmTasks('grunt-contrib-uglify');
	grunt.registerTask('default', taskArray);
}