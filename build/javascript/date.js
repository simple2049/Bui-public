/*时间日期控件
 * @Author: kevin.huang 
 * @Date: 2018-07-21 12:28:05 
 * @Last Modified by: kevin.huang
 * @Last Modified time: 2019-03-29 15:21:10
 * Copyright (c): kevin.huang Released under MIT License
 */
(function (global, factory) {
    if (typeof define === 'function' && define.amd) {
        define(['$B'], function (_$B) {
            return factory(global, _$B);
        });
    } else {
        if (!global["$B"]) {
            global["$B"] = {};
        }
        factory(global, global["$B"]);
    }
}(typeof window !== "undefined" ? window : this, function (window, $B) {
    var monthArr = ["一月", "二月", "三月", "四月", "五月", "六月", "七月", "八月", "九月", "十月", "十一月", "十二月"];
   var dayArr = ['日', '一', '二', '三', '四', '五', '六'];
  function MyDate(jqObj, opts) {
      $B.extend(this, MyDate); //继承父类 
     
  }
  MyDate.prototype = {
  };
  $B["Calendar"] = MyDate;
  return MyDate;
}));