/*
 * 基础封装 Ajax 、HashTable、BaseControl、String扩展
 * @Author: kevin.huang
 * @Date: 2018-07-21 09:09:53 
 * @Last Modified by: kevin.huang
 * @Last Modified time: 2019-05-07 20:42:25
 * Copyright (c): kevin.huang Released under MIT License
 */
/**
 * 2019-05-03 bindForm 支持calendar时间日期控件
 * ***/
(function (global, factory) {
    if (typeof define === 'function' && define.amd && !window["_all_in_"]) {
        define(['jquery', 'config'], function ($, c) {
            return factory(global, $, c);
        });
    } else {
        factory(global, $);
    }
}(typeof window !== "undefined" ? window : this, function (window, $, cfg) {
    "use strict";
    var $B = window["$B"] ? window["$B"] : {};
    window["$B"] = $B;
    var config = $B.config;
    var document = window.document;
    var charSpan = "<span style='position:absolute;white-space:nowrap;top:-10000000px;left:-10000000px' id='{id}'></span>",
        $body,
        _char_id_ = "__getcharwidth__",
        _ellipsis_char_id = "_ellipsis_char_",
        $spen_CharWidth,
        $ellipsisCharDiv = null,
        loadingHtml = "<div style='padding-left:16px;' class='loading'>" + config.loading + "</div>",
        chars = ['0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z'];


    /**定义一个BaseControl,封装一些共有方法***/
    function BaseControl() {       
        this._version = '_ver_var_';       
        this._release = '_ver_date_';
        this._author = '_ver_site_';
    }
    BaseControl.prototype = {
        constructor: BaseControl,
        version: function () {
            $B.debug(this._version);
            $B.debug(this._release);
            $B.debug(this._author);
        },
        /*** ajax请求
         args={
                async: true,
                url:'',
                data:{},
                ok:function(data,message){},
                fail:function(message){},
                final:function(res){}
        }
        ***/
        ajax: function (args) {
            $B.request(args);
        },
        debug: function (msg) {
            $B.debug(msg);
        },
        error: function (err) {
            $B.debug("error:" + err);
        },
        clear: function () {
            if (this.jqObj) {
                this.jqObj.children().remove();
            }
            this.delProps();
        },
        /**
         * 注册一个函数
         * **/
        regiterFn: function (fnName, fn) {
            this[fnName] = fn;
        },
        destroy: function () {
            if (this.jqObj) {
                this.jqObj.remove();
            }
            this.delProps();
        },
        /***解除所有私有引用***/
        delProps: function () {
            for (var p in this) {
                if (this.hasOwnProperty(p)) {
                    delete this[p];
                }
            }
        }
    };

    function _getBody() {
        if (!$body) {
            $body = $(document.body).css("position", "relative");
        }
        return $body;
    }
    var removeClearTimer;
    var TextEvents = {
        input: function () {
            var $t = $(this);
            if ($t.val() !== "") {
                var $b = _getBody();
                var btn = $b.children("#k_text_clear_btn");
                if (btn.length === 0 || btn.data("target") !== this) {
                    TextEvents.mouseover.call(this);
                }
            }
        },
        mouseover: function () {
            clearTimeout(removeClearTimer);
            var $t = $(this);
            if (!$t.attr("readonly")) {
                var $b = _getBody();
                var clrBtn = $b.children("#k_text_clear_btn").hide();
                if ($t.val() !== "") {
                    var ofs = $t.offset();
                    var left = $t.outerWidth() + ofs.left - 15;
                    var top = ($t.outerHeight() / 2) + ofs.top - 12;
                    if ($t.hasClass("k_combox_input")) {
                        left = left - 18;
                    }
                    if (clrBtn.length === 0) {
                        clrBtn = $("<div id='k_text_clear_btn' style='cursor:pointer;position:absolute;top:" + top + "px;left:" + left + "px;width:14px;height:14px;z-index:2147483647;'><i style='color:#C1C1C1' class='fa fa-cancel-2'></i></div>");
                        clrBtn.appendTo($b).on({
                            click: function () {
                                var $input = $(this).data("target");
                                $input.val("");
                                $input.trigger("input.mvvm");
                                $(this).hide();
                                $input.focus();
                            }
                        }).data("target", $t);
                    } else {
                        clrBtn.data("target", $t).css({ top: top, left: left }).show();
                    }
                }
            }
        },
        mouseout: function (e) {
            var $b = _getBody();
            removeClearTimer = setTimeout(function () {
                $b.children("#k_text_clear_btn").hide();
            }, 800);
        }
    };
    var ajaxOpts = {
        timeout: 2000 * 60,
        type: "POST",
        dataType: 'json',
        async: true,
        error: function (xhr, status, errorThrown) {
            var res = {
                message: config.permission + xhr.status
            };
            try {
                res = eval('(' + xhr.responseText + ')');
            } catch (e) {
                if (window.console) {
                    console.log(xhr.responseText);
                }
            }
            if (xhr.status === 200) {
                this.success(res);
            } else {
                $B.error(config.requestError);
                // if ($body.children("#_request_error_window").length === 0) {
                //     var win = $B.error(res.message);
                //     win.setAttr({
                //         "id": "_request_error_window"
                //     });
                // }
            }
            this.recoverButton();
            this.final(status);
        },
        success: function (res) {
            this.recoverButton();
            this.final(res);
            if (typeof res.code !== "undefined") {
                if (res.code === 0) {
                    var data = res.data;
                    if (res.strConvert) {
                        data = eval('(' + res.data + ')');
                    }
                    this.ok(res.message, data,res);
                } else if (res.code === 99999) {
                    if (res.data === "notlogin") {
                        $B.error(res.message);
                        setTimeout(function () {
                            if (window.ctxPath) {
                                window.top.location = $B.getHttpHost(window.ctxPath);
                            }
                        }, 1600);
                    } else {
                        $B.error(config.permission);
                    }
                } else {
                    this.fail(res.message, res);
                }
            } else {
                this.ok(res, res);
            }
        },
        /**
         *当返回结果是正确时的处理
         *data:返回的数据
         *message:提示的信息
         **/
        ok: function (message, data) { },
        recoverButton: function () {
            if (this["target"]) {
                this["target"].removeAttr("disabled");
                if (this["target"][0].tagName === "INPUT") {
                    this["target"].val(this.recoverText);
                } else {
                    this["target"].html(this.recoverText);
                }
                this["target"] = undefined;
            }
        },
        /***
         *当返回结果是非正确时的处理
         ***/
        fail: function (msg, res) {
            if (_getBody().children("#_request_fail_window").length === 0) {
                var win = $B.alert(msg);
                win.setAttr({
                    "id": "_request_fail_window"
                });
            }
        },
        /**
         * 无论如何都回调的函数
         ****/
        final: function (res) { //无论成功，失败，错误都执行的回调
        }
    };
    /****原生扩展******/
    //-------------------------------------
    //十六进制颜色值的正则表达式
    var reg = /^#([0-9a-fA-f]{3}|[0-9a-fA-f]{6})$/;
    /*RGB颜色转换为16进制*/
    String.prototype.toHexColor = function () {
        var that = this;
        if (/^(rgb|RGB)/.test(that)) {
            var aColor = that.replace(/(?:\(|\)|rgb|RGB)*/g, "").split(",");
            var strHex = "#";
            for (var i = 0; i < aColor.length; i++) {
                var hex = Number(aColor[i]).toString(16);
                if (hex.length === 1) {
                    hex = "0" + hex;
                }
                if (hex === "0") {
                    hex += hex;
                }
                strHex += hex;
            }
            if (strHex.length !== 7) {
                strHex = that;
            }
            return strHex.toUpperCase();
        } else if (reg.test(that)) {
            var aNum = that.replace(/#/, "").split("");
            if (aNum.length === 6) {
                return that.toUpperCase();
            } else if (aNum.length === 3) {
                var numHex = "#";
                for (var j = 0; j < aNum.length; j += 1) {
                    numHex += (aNum[j] + aNum[j]);
                }
                return numHex.toUpperCase();
            }
        } else {
            return that.toUpperCase();
        }
    };
    //-------------------------------------------------
    /*16进制颜色转为RGB格式*/
    String.prototype.toRgbColor = function () {
        var sColor = this.toLowerCase();
        if (sColor && reg.test(sColor)) {
            if (sColor.length === 4) {
                var sColorNew = "#";
                for (var i = 1; i < 4; i += 1) {
                    sColorNew += sColor.slice(i, i + 1).concat(sColor.slice(i, i + 1));
                }
                sColor = sColorNew;
            }
            //处理六位的颜色值
            var sColorChange = [];
            for (var j = 1; j < 7; j += 2) {
                sColorChange.push(parseInt("0x" + sColor.slice(j, j + 2)));
            }
            return "RGB(" + sColorChange.join(",") + ")";
        } else {
            return sColor.toUpperCase();
        }
    };
    Array.prototype.unique = function () {
        this.sort();
        var re = [this[0]];
        for (var i = 1; i < this.length; i++) {
            if (this[i] !== re[re.length - 1]) {
                re.push(this[i]);
            }
        }
        return re;
    };
    Date.prototype.format = function (format) {
        var date = {
            "M+": this.getMonth() + 1,
            "d+": this.getDate(),
            "h+": this.getHours(),
            "m+": this.getMinutes(),
            "s+": this.getSeconds(),
            "q+": Math.floor((this.getMonth() + 3) / 3),
            "S+": this.getMilliseconds()
        };
        if (/(y+)/i.test(format)) {
            format = format.replace(RegExp.$1, (this.getFullYear() + '').substr(4 - RegExp.$1.length));
        }
        for (var k in date) {
            if (new RegExp("(" + k + ")").test(format)) {
                format = format.replace(RegExp.$1, RegExp.$1.length === 1 ? date[k] : ("00" + date[k]).substr(("" + date[k]).length));
            }
        }
        return format;
    };
    String.prototype.trim = function () {
        return this.replace(/(^\s*)|(\s*$)/g, "");
    };
    String.prototype.leftTrim = function () {
        return this.replace(/(^\s*)/g, "");
    };
    String.prototype.rightTrim = function () {
        return this.replace(/(\s*$)/g, "");
    };
    /**HashTable**/
    function HashTable() {
        this.size = 0;
        this.entry = {};
        var i, len, attr;
        if (typeof this.add !== 'function') {
            HashTable.prototype.add = function (key, value) {
                if (!this.containsKey(key)) {
                    this.size++;
                }
                this.entry[key] = value;
            };
        }
        if (typeof this.getValue !== 'function') {
            HashTable.prototype.getValue = function (key) {
                return this.containsKey(key) ? this.entry[key] : null;
            };
        }
        if (typeof this.remove !== 'function') {
            HashTable.prototype.remove = function (key) {
                if (this.containsKey(key) && (delete this.entry[key])) {
                    this.size--;
                }
            };
        }
        if (typeof this.containsKey !== 'function') {
            HashTable.prototype.containsKey = function (key) {
                return (key in this.entry);
            };
        }
        if (typeof this.containsValue !== 'function') {
            HashTable.prototype.containsValue = function (value) {
                var pkeys = Object.keys(this.entry);
                for (i = 0, len = pkeys.length; i < len; ++i) {
                    attr = pkeys[i];
                    if (this.entry[attr] === value) {
                        return true;
                    }
                }
                return false;
            };
        }
        if (typeof this.getValues !== 'function') {
            HashTable.prototype.getValues = function () {
                var values = [];
                var pkeys = Object.keys(this.entry);
                for (i = 0, len = pkeys.length; i < len; ++i) {
                    attr = pkeys[i];
                    values.push(this.entry[attr]);
                }
                return values;
            };
        }
        if (typeof this.getKeys !== 'function') {
            HashTable.prototype.getKeys = function () {
                var keys = Object.keys(this.entry);
                return keys;
            };
        }
        if (typeof this.getSize !== 'function') {
            HashTable.prototype.getSize = function () {
                return this.size;
            };
        }
        if (typeof this.clear !== 'function') {
            HashTable.prototype.clear = function () {
                this.size = 0;
                this.entry = {};
            };
        }
        if (typeof this.joinValue !== 'function') {
            HashTable.prototype.joinValue = function (key, value, split) {
                var sp = typeof split === 'undefined' ? ',' : split;
                var existValue = this.getValue(key);
                if (existValue === null) {
                    this.add(key, value);
                    this.size++;
                } else {
                    var arr = existValue.split(sp);
                    arr.push(value);
                    this.add(key, arr.join(sp));
                }
            };
        }
        if (typeof this.destroy !== 'function') {
            HashTable.prototype.destroy = function () {
                this.size = 0;
                this.entry = null;
            };
        }
        if (typeof this.each !== 'function') {
            HashTable.prototype.each = function (fn) {
                var pkeys = Object.keys(this.entry);
                for (i = 0, len = pkeys.length; i < len; ++i) {
                    attr = pkeys[i];
                    fn(attr, this.entry[attr]);
                }
            };
        }
        if (typeof this.getJson !== 'function') {
            HashTable.prototype.getJson = function (fn) {
                return $.extend(true, {}, this.entry);
            };
        }
    }
    $B["HashTable"] = HashTable;

    /**静态API**/
    $.extend($B, {
        /**框架的ajax统一入口
         *所有ajax返回均以 res={code:'',message:'',data:{}}的格式返回
        *code=0表示服务器无异常运行并返回结果，code=1时，表示服务器出现异常并返回提示
        *message，用与服务器返回的信息提示
        *data,用于服务器返回的数据，如tree组件、datagrid组件返回的数据就保存到data当中
        args={
                timeout: 1000 * 60, //超时
                type: "POST",       //请求方式
                dataType: 'json',   //请求数据类型
                async: true,        //是否异步
                preRequest: fn,     //请求前，回调
                url:'',             //请求url
                data:{},            //请求参数
                ok:function(message,data){},    //成功回调，message:返回信息，data:返回数据
                fail:function(message){},       //失败回调，message:返回信息
                final:function(res){}           //无论成功/失败都调用的回调 res = {code:'',message:'',data:{}}
        }
        ****/
        request: function () {
            var args = arguments[0];
            var opts;
            if (args !== undefined) {
                opts = $.extend({}, ajaxOpts, args);
            } else {
                opts = ajaxOpts;
            }
            if (typeof opts.preRequest === 'function') {
                opts.preRequest();
            }
            //剔除值为null的参数，null表示不需要更新到数据库
            for (var prop in opts.data) {
                if (opts.data[prop] === null) {
                    delete opts.data[prop];
                } else {
                    opts.data[prop] = this.htmlEncode(opts.data[prop]);
                    //opts.data[prop] = opts.data[prop];
                }
            }
            var queue = window["_submit_queues"];
            var submitBtn;
            if (queue && queue.length > 0) {
                var lastIdx = queue.length - 1;
                var diff = queue[lastIdx].date - new Date();
                if (diff <= 500) {//500毫秒内
                    submitBtn = queue[lastIdx].btn;
                    var q = queue.shift();
                    q.btn = undefined;
                }
            }
            if (arguments.length > 1 || submitBtn) {
                var btn = arguments.length > 1 ? arguments[1] : submitBtn;
                opts["target"] = btn;
                btn.attr("disabled", "disabled");
                if (btn[0].tagName === "INPUT") {
                    opts["recoverText"] = btn.val();
                    btn.val(config.busy);
                } else {
                    opts["recoverText"] = btn.html();
                    btn.html(config.busy);
                    btn.prepend("<i style='padding:0;margin-right:3px;' class='fa fa-spin fa-spin6'></i>");
                }
            }
            $.ajax(opts);
        },
        /***
         * 深色、浅色换转 
         * col 16进制颜色
         * amt自定义变暗参数，< 0为变暗，>0为变亮
         * ***/
        lightenDarkenColor: function (col, amt) {
            var usePound = false;
            if (col[0] === "#") {
                col = col.slice(1);
                usePound = true;
            }
            var num = parseInt(col, 16);
            var r = (num >> 16) + amt;
            if (r > 255) {
                r = 255;
            } else if (r < 0) {
                r = 0;
            }
            var b = ((num >> 8) & 0x00FF) + amt;
            if (b > 255) {
                b = 255;
            } else if (b < 0) {
                b = 0;
            }
            var g = (num & 0x0000FF) + amt;
            if (g > 255) {
                g = 255;
            } else if (g < 0) {
                g = 0;
            }
            return (usePound ? "#" : "") + String("000000" + (g | (b << 8) | (r << 16)).toString(16)).slice(-6);
        },
        /**
         * 判断颜色是否是浅色，深色
         * ***/
        isContrastYIQ: function (_color) {
            var colorrgb = _color;
            if (_color.indexOf("#") > -1) {
                colorrgb = _color.toRgbColor(); //colorRgb(hexcolor);
            }
            var colors = colorrgb.match(/^rgb\((\d+),\s*(\d+),\s*(\d+)\)$/);
            var red = colors[1];
            var green = colors[2];
            var blue = colors[3];
            var brightness;
            brightness = (red * 299) + (green * 587) + (blue * 114);
            brightness = brightness / 255000;
            if (brightness >= 0.5) {
                return "light";
            } else {
                return "dark";
            }
        },
        /**
         * 用于提交期间，改变按钮的状态，防止重复提交
         * ***/
        changeButtonStatus: function (button) {
            var i = button.children("i");
            var clazz;
            if (button.attr("disabled")) {
                button.removeAttr("disabled");
                clazz = button.data("clazz");
                button.removeData("clazz").removeClass("k_toolbar_button_disabled");
                i.attr("class", clazz);
            } else {
                button.prop("disabled", true);
                clazz = i.attr("class");
                button.data("clazz", clazz).addClass("k_toolbar_button_disabled");
                i.attr("class", "fa fa-spin6 fa-spin");
            }
        },
        getHttpHost: function (ctxPath) {
            var proto = window.location.protocol;
            var host = proto + "//" + window.location.host;
            var ctx;
            if (!ctxPath && window.ctxPath) {
                ctx = window.ctxPath;
            } else if (ctxPath) {
                ctx = ctxPath;
            }
            if (ctx) {
                host = host + ctx;
            }
            return host;
        },
        htmlEncode: function (str) {
            if (!str || typeof str.replace === "undefined") {
                return str;
            }
            var s = "";
            if (str.length === 0) {
                return "";
            }
            // s = str.replace(/&/g,"&amp;");
            s = str.replace(/</g, "&lt;");
            s = s.replace(/>/g, "&gt;");
            s = s.replace(/eval\((.*)\)/g, "");
            s = s.replace(/<.*script.*>/, "");
            /*双引号 单引号不替换
            s = s.replace(/\'/g,"&#39;");
            s = s.replace(/\"/g,"&quot;");*/
            return s;
        },
        htmlDecode: function (str) {
            if (typeof str.replace === "undefined") {
                return str;
            }
            var s = "";
            if (str.length === 0) {
                return "";
            }
            s = str.replace(/&amp;/g, "&");
            s = s.replace(/&lt;/g, "<");
            s = s.replace(/&gt;/g, ">");
            s = s.replace(/&#39;/g, "\'");
            s = s.replace(/&quot;/g, "\"");
            return s;
        },
        /**获取元素旋转后的位置偏移量**/
        getAnglePositionOffset: function (el) {
            var matrix = el.css("transform");
            var ofs = { fixTop: 0, fixLeft: 0 };
            if (matrix && matrix !== "none") {
                var pos = el.position();
                var angle = $B.getMatrixAngle(matrix);
                if (angle !== 0) {
                    var clone = $("<div />");
                    var style = el.attr("style");
                    clone.attr("style", style).css({ "filter": "alpha(opacity=0)", "-moz-opacity": "0", "opacity": "0", "position": "absolute", "z-index": -111 });
                    clone.css("transform", "rotate(0deg)");
                    clone.appendTo(el.parent());
                    var clonePos = clone.position();
                    ofs.fixTop = clonePos.top - pos.top;
                    ofs.fixLeft = -(pos.left - clonePos.left);
                    clone.remove();
                }
            }
            return ofs;
        },
        /**
         * 获取旋转的角度
         * matrix = css("transform")
         * **/
        getMatrixAngle: function (matrix) {
            if (matrix === "none") {
                return 0;
            }
            var values = matrix.split('(')[1].split(')')[0].split(',');
            var a = values[0];
            var b = values[1];
            var angle = Math.round(Math.atan2(b, a) * (180 / Math.PI));
            return angle;
        },
        /**
         * debug日志
         * ***/
        debug: function (message) {
            console.log("debug:" + message);
        },
        /**
         * 继承实现，child子类，parent父类，parent不传则默认为baseControl
         * _this 子类this, 
         * child 子类构造函数, 
         * parent 需要继承的父类构造函数,
         * args 参数
         * ****/
        extend: function (_this, child, parent, args) {
            if (!parent) {
                parent = BaseControl;
            }
            /***私有继承***/
            parent.call(_this, args);
            /***拷贝prototype继承***/
            Object.keys(parent.prototype).forEach(function (key) {
                var prop = child.prototype[key];
                if (!prop) { //如果子类已经存在，则不继承,模拟重写
                    child.prototype[key] = parent.prototype[key];
                }
            });
            child.prototype.constructor = child;
            //开放子类调用基类
            _this["super"] = new parent();
        },
        scrollbar: function (_this) {
            if (_this.mCustomScrollbar) {

            } else {
                _this.css("overflow", "auto");
            }
        },
        /**获取一个HashTable**/
        getHashTable: function () {
            return new HashTable();
        },
        /**是否是url**/
        isUrl: function (str) {
            return /^((http(s)?|ftp):\/\/)?([\w-]+\.)+[\w-]+(\/[\w- .\/?%&=]*)?(:\d+)?/.test(str);
        },
        /***
         * 写cookie
         * ***/
        writeCookie: function (name, value, expiredays) {
            if (!this.isNotEmpty(expiredays)) {
                expiredays = 1;
            }
            try {
                var exdate = new Date();
                exdate.setDate(exdate.getDate() + expiredays);
                document.cookie = name + "=" + window.escape(value) + ";expires=" + exdate.toGMTString();
            } catch (ex) {
            }
        },
        /**
         * 读取cookie
         * ***/
        getCookie: function (c_name) {
            if (document.cookie.length > 0) {
                var c_start = document.cookie.indexOf(c_name + "=");
                if (c_start !== -1) {
                    c_start = c_start + c_name.length + 1;
                    var c_end = document.cookie.indexOf(";", c_start);
                    if (c_end === -1) {
                        c_end = document.cookie.length;
                    }
                    return window.unescape(document.cookie.substring(c_start, c_end));
                }
            }
            return "";
        },
        isIE: function () {
            var userAgent = navigator.userAgent.toLowerCase();
            return userAgent.indexOf("rv:11.0") > 0 || /msie/.test(userAgent);
        },
        isNotEmpty: function (v) {
            return v !== null && typeof v !== 'undefiend' && v !== "";
        },
        /**获取滚动条宽度**/
        getScrollWidth: function () {
            var noScroll, scroll, oDiv = document.createElement("DIV");
            oDiv.style.cssText = "position:absolute; top:-1000px; width:100px; height:100px; overflow:hidden;";
            noScroll = document.body.appendChild(oDiv).clientWidth;
            oDiv.style.overflowY = "scroll";
            scroll = oDiv.clientWidth;
            document.body.removeChild(oDiv);
            return noScroll - scroll;
        },
        /***
         * 创建高性能的延时运行函数（避免频繁执行）
         * func:需要延时运行的函数
         * wait:等待时间 毫秒
         * immediate:是否立即执行
         * 返回一个执行函数
         * ***/
        delayFun: function (func, wait, immediate) {
            var timeout;
            return function () {
                var context = this,
                    args = arguments;
                var later = function () {
                    timeout = null;
                    if (!immediate) {
                        func.apply(context, args);
                    }
                };
                var callNow = immediate && !timeout;
                clearTimeout(timeout);
                timeout = setTimeout(later, wait);
                if (callNow) {
                    func.apply(context, args);
                }
            };
        },
        /***
         * 对一个tag标签定义一个信息提示框
         * target:目标标签
         * message:提示的信息
         * timeout:自动消失时间，可以不填
         * return 返回提示框对象本身，可以需要关闭可以用 returnObj.hide() / returnObj.show() /returnObj.remove()
         * ***/
        tagMessage: function (target, message, timeout) { },
        /**
         * 获取当前url中参数，并以对象形式返回
         * @param strUrl 需要获取的url，如果不传则为当前打开页面的url
         * ***/
        getUrlParams: function (strUrl) {
            var url;
            if (strUrl !== undefined) {
                url = strUrl;
            } else {
                url = location.search;
            }
            var params = {};
            var index = url.indexOf("?");
            if (index !== -1) {
                var str = url.substr(index + 1);
                var strs = str.split("&");
                for (var i = 0; i < strs.length; i++) {
                    params[strs[i].split("=")[0]] = unescape(strs[i].split("=")[1]);
                }

            }
            /** 
            var obj = {};            
            var params = window.location.search.substr(1);
            //[^&=]+ 表示不含&或=的连续字符，加上()就是提取对应字符串
            url.replace(/([^&=]+)=([^&=]*)/gi,function(rs,$1,$2){
                obj[$1] =  decodeURIComponent($2);
            });
            return obj;
            **/
            return params;
        },
        /***
         * 获取当前鼠标的位置
         ***/
        mouseCoords: function (ev) {
            if (ev.pageX || ev.pageY) {
                return {
                    x: ev.pageX,
                    y: ev.pageY
                };
            } else {
                return {
                    x: ev.clientX + document.body.scrollLeft - document.body.clientLeft,
                    y: ev.clientY + document.body.scrollTop - document.body.clientTop
                };
            }
        },
        getEllipsisChar: function (txt, fs, width, height) {
            if ($ellipsisCharDiv === null) {
                $ellipsisCharDiv = _getBody().children("#" + _ellipsis_char_id);
                if ($ellipsisCharDiv.length === 0) {
                    $ellipsisCharDiv = $("<div style='height:" + height + "px;width:" + width + "px;position:absolute;top:0;z-index:0;overflow:auto;top:-1000px;' id='" + _ellipsis_char_id + "'></div>").appendTo(_getBody());
                }
            }
            $ellipsisCharDiv.css("font-size", fs).html("");
            var len = txt.length,
                i = 0;
            var el = $ellipsisCharDiv[0];
            for (; i < len; i++) {
                el.innerHTML = txt.substr(0, i);
                if (el.scrollHeight > height) {
                    el.innerHTML = txt.substr(0, i - 2);
                    break;
                }
            }
            return el.innerHTML + "...";
        },
        /***
         *获取字符长度
         *@param text 文本
         *@param fs 文本的字体大小
         **/
        getCharWidth: function (text, fs) {
            if (typeof fs === 'undefined') {
                fs = $B.config.fontSize;
            }
            if (!$spen_CharWidth) {
                $spen_CharWidth = $(charSpan.replace(/{id}/, _char_id_)).appendTo(_getBody());
            }
            $spen_CharWidth.css({
                'font-size': fs
            });
            var w = 20;
            try {
                $spen_CharWidth.html(this.htmlEncode(text));
                w = $spen_CharWidth.outerWidth();
                setTimeout(function () {
                    $spen_CharWidth.html("");
                }, 1);
            } catch (ex) {
                this.error(ex);
            }
            return w;
        },
        getUUID: function () {
            return this.generateDateUUID();
        },
        generateDateUUID: function (fmt, count) {
            var c = count ? count : 12;
            var formt = fmt ? fmt : "yyyyMMddhhmmss";
            var prex = (new Date()).format(formt);
            return prex + this.generateMixed(c);
        },
        /***产生混合随机数
         *@param n 位数 默认6
         ***/
        generateMixed: function (n) {
            var _n = n ? n : 6;
            var res = [];
            for (var i = 0; i < _n; i++) {
                var id = Math.ceil(Math.random() * 35);
                res.push(chars[id]);
            }
            return res.join("");
        },
        /*** 某个html标签加载远程html文件
         options={  target:jquery目标对象,
                    url:'远程地址',
                    params:{},//参数
                    preload:function(){.........} , //加载前处理事件
                    onLoaded:function(result){.........}  //加载后处理事件
        } ***/
        htmlLoad: function () {
            var opts = arguments[0];
            opts.target.children().remove();
            opts.target.html(loadingHtml);
            if (typeof opts.preload === 'function') {
                opts.preload.call(opts.target);
            }
            var url = opts.url;
            if (url.indexOf("?") > 0) {
                url = url + "&_c_1=" + this.generateMixed(5);
            } else {
                url = url + "?_c_1=" + this.generateMixed(5);
            }
            opts.target.load(url, opts.params, function (xmlReq, statu, error) {
                if (statu === 'error') {
                    if (xmlReq) {
                        var re = new RegExp("<body>(.+)</body>", "gi");
                        var res = re.exec(xmlReq);
                        if (res.length >= 2) {
                            opts.target.html(res[1]);
                        } else {
                            opts.target.html(xmlReq);
                        }
                    } else {
                        var addr = $B.getHttpHost();
                        if (addr.indexOf("file:") >= 0) {
                            opts.target.html($B.config.crossError);
                        } else {
                            opts.target.html($B.config.htmlLoadError);
                        }
                    }
                } else {
                    if (typeof opts.onLoaded === 'function') {
                        opts.onLoaded.call(opts.target);
                    }
                }
            });
        },
        /**获取jquery对象
         * idOrTag
         * ***/
        getJqObject: function (idOrTag) {
            if (typeof idOrTag === 'string') {
                return $(idOrTag);
            } else {
                return idOrTag;
            }
        },
        /**
         *将form表单转为json对象
         *@param form 表单的容器如div、table、form
         *@param entityJson 实体json
         ***/
        parseForm: function (form, entityJson) {
            var _this = this;
            var formWrap = this.getJqObject(form);
            function objectToKeyValuePair($obj, _hash) {
                var _key = $obj.attr("id");
                if (!_key) {
                    _key = $obj.attr("name");
                }
                if (_key !== undefined) {
                    var _val = $obj.val();
                    _hash.add(_key, _val);
                    return _key + '=' + _val; //返回值，如果需要的话
                } else {
                    return null;
                }
            }
            var allText = formWrap.find("input[type=text]");
            var allPwdText = formWrap.find("input[type=password]");
            var allAreatext = formWrap.find("textarea");
            var allRadio = formWrap.find("input[type=radio]");
            var allCheckbox = formWrap.find("input[type=checkbox]");
            var allSelect = formWrap.find("select");
            var allHidden = formWrap.find("input[type=hidden]");
            var allFileText = formWrap.find("input[type=file]");
            var hash = this.getHashTable();
            //所有隐藏域
            $.each(allHidden, function (idx, obj) {
                objectToKeyValuePair($(obj), hash);
            });
            //所有密码框
            $.each(allPwdText, function (idx, obj) {
                objectToKeyValuePair($(obj), hash);
            });
            //查找所有文本框
            $.each(allText, function (idx, obj) {
                objectToKeyValuePair($(obj), hash);
            });
            //查找所有文件框
            $.each(allFileText, function (idx, obj) {
                objectToKeyValuePair($(obj), hash);
            });
            //查找所有多行文本
            $.each(allAreatext, function (idx, obj) {
                objectToKeyValuePair($(obj), hash);
            });
            /*****Radio******/
            $.each(allRadio, function (idx, obj) {
                hash.add($(obj).attr("name"), null); //先加键，value设空
            });
            $.each(allRadio, function (idx, obj) {
                var $t = $(obj);
                if ($t.is(":checked")) {
                    hash.joinValue($t.attr("name"), $t.val()); //设置值
                }
            });
            /*****Checkbox存在多选情况******/
            if (allCheckbox.length > 0) {
                var tempData = {};
                $.each(allCheckbox, function (idx, obj) {
                    var $obj = $(obj);
                    var _n = $obj.attr("name");
                    if (!tempData[_n]) {
                        tempData[_n] = []; //先加键，value设空
                    }
                    if ($obj.is(":checked")) {
                        tempData[_n].push($obj.val());
                    }
                });
                for (var key in tempData) {
                    if (this.hasOwnProperty(key)) {
                        var _v = tempData[key].join(',');
                        if (_v !== '') {
                            hash.add(key, _v); //设置值
                        }
                    }
                }
            }
            //查找所有下拉列表
            $.each(allSelect, function (idx, obj) {
                var $t = $(obj);
                if ($t.attr("multiple") === true) {
                    var allvalue = [];
                    hash.add($t.attr("name"), ''); //先加键，value设空
                    var sels = $t.children("option[selected]");
                    $.each(sels, function (j, o) {
                        allvalue.push($(o).value);
                    });
                    hash.joinValue($t.attr("name"), allvalue.join(',')); //设置值
                } else {
                    objectToKeyValuePair($t, hash);
                }
            });
            var objJson = hash.getJson();
            hash.destroy();
            if (entityJson) {
                return $.extend(true, {}, entityJson, objJson);
            } else {
                return objJson;
            }
        },
        /***实现双向绑定的表单填充
         *@param form 待填充的表单，如table、form
         *@param dataObj 填充数据，JSON对象
         *@param onchangeFn 
         *@return:返回具有双向联动能力的数据对象
         ***/
        bindForm: function (form, dataObj, onchangeFn) {
            var copyData = dataObj;//$.extend(true,{},dataObj);
            var oldFiledMap = {};
            if ($B.Mvvm) {
                var allText = form.find("input[type=text]");
                var bindExpressFn = function ($tag, setExprssFn) {
                    var prop = $tag.attr("id");
                    if (!prop) {
                        prop = $tag.attr("name");
                    }
                    if (typeof dataObj[prop] !== "undefined") {
                        setExprssFn($tag, prop);
                    }
                    //处理新旧验证
                    if (prop.indexOf("old_") === 0) {
                        var srProp = prop.replace("old_", "");
                        var v = dataObj[srProp];
                        $tag.val(v);
                        oldFiledMap[prop] = v;
                    }
                };
                allText.each(function () {
                    var $txt = $(this);
                    bindExpressFn($txt, function ($tag, prop) {
                        if ($tag.hasClass("k_combox_input")) {//对combox进行默认支持
                            $tag.attr("watcher", "kcomboxWatcher").attr("express", "{{this.kcomboxExpress(this.data." + prop + ",el)}}");
                        }else if ($tag.hasClass("k_calendar_input")) {//2019-05-03对时间日期控件进行支持
                            $tag.attr("watcher", "kcalendarWatcher").attr("express", "{{this.kcalendarExpress(this.data." + prop + ",el)}}");
                        }else {
                            $tag.attr("value", "{{this.data." + prop + "}}");
                        }
                    });
                });
                var allPwdText = form.find("input[type=password]");
                allPwdText.each(function () {
                    var $txt = $(this);
                    bindExpressFn($txt, function ($tag, prop) {
                        $tag.attr("value", "{{this.data." + prop + "}}");
                    });
                });

                var allHidden = form.find("input[type=hidden]");
                allHidden.each(function () {
                    var $txt = $(this);
                    bindExpressFn($txt, function ($tag, prop) {
                        $tag.attr("value", "{{this.data." + prop + "}}");
                    });
                });

                var allAreatext = form.find("textarea");
                allAreatext.each(function () {
                    var $txt = $(this);
                    bindExpressFn($txt, function ($tag, prop) {
                        $tag.text("{{this.data." + prop + "}}");
                    });
                });
                var allRadio = form.find("input[type=radio]");
                allRadio.each(function () {
                    var r = $(this);
                    var name = r.attr("name");
                    var $label = r.parent();
                    if ($label.hasClass("k_radio_label") && typeof dataObj[name] !== "undefined") {
                        $label.attr("watcher", "kRadioWatcher").attr("express", "{{this.kRadioExpress(this.data." + name + ",el)}}");
                    }
                });

                var allCheckbox = form.find("input[type=checkbox]");
                allCheckbox.each(function () {
                    var r = $(this);
                    var name = r.attr("name");
                    var $label = r.parent();
                    if ($label.hasClass("k_checkbox_label") && typeof dataObj[name] !== "undefined") {
                        $label.attr("watcher", "kcheckBoxWatcher").attr("express", "{{this.kcheckBoxExpress(this.data." + name + ",el)}}");
                    }
                });

                var allSelect = form.find("select");
                allSelect.each(function () {
                    var $ele = $(this);
                    var id = $ele.attr("id");
                    if (dataObj[id]) {
                        var optsHtml = [];
                        $ele.children().each(function () {
                            var opt = $(this);
                            var v = opt.val();
                            var txt = opt.text();
                            var sle = "{{this.data." + id + " === '" + v + "' ? true:false}}";
                            var optHtml = '<option value="' + v + '" selected="' + sle + '">' + txt + '</option>';
                            optsHtml.push(optHtml);
                        });
                        $ele.html(optsHtml.join(""));
                    }
                });
                //var allFileText = form.find("input[type=file]");
                //树形控件支持
                form.find("ul.k_tree_root").each(function(){
                    bindExpressFn($(this), function ($tag, prop) {
                        $tag.attr("watcher", "ktreeWatcher").attr("express", "{{this.ktreeExpress(this.data." + prop + ",el)}}");
                    });                    
                });
                var vm = new $B.Mvvm({
                    el: form[0],
                    data: copyData,
                    onChanged: onchangeFn,
                    onGetJson: function (json) {
                        $.extend(json, oldFiledMap);
                    }
                });
                window["curMvvm"] = vm;
                return vm;
            }

        },
        /***
         *将json对象填充到容器中，与bindForm有区别，fillView只是将内容填充到对应的区域
         *这里填充的是任意元素，常用于填充到详情显示页面
         *@param wrap 待填充的容器对象
         *@param dataObj 填充数据，JSON对象
         ***/
        fillView: function (wrap, dataObj) {
            for (var attr in dataObj) {
                if (this.hasOwnProperty(attr)) {
                    var value = dataObj[attr];
                    if (value === null || value === undefined) {
                        value = "";
                    }
                    var $html = wrap.find("#" + attr);
                    if ($html.length > 0) {
                        if ($html[0].tagName === 'INPUT') {
                            $html.value(value);
                        } else {
                            $html.text(value);
                        }
                    }
                }
            }
        },
        /***
         *重置表单
         *@param form 需要重置的表单元素外包对象
         *@param defData 重置表单时候的默认数据，如果某个元素没有传默认值，则为空
         *defData 可以不传
         ****/
        resetForm: function (form, defData) {
        },
        /**
        * 简单下拉列表（原生的select）
         options={
             target:id/对象,
             data:option数据项,
             idField:'option的value字段',	
             textField:'option显示的字段',	
             defaultVal:'选择的项目的值',	  	
             onchange:fn(选择的option) //选择触发函数事件
         }
        *****/
        simpalSelect: function (options) {
            var target;
            if (typeof options.target === 'string') {
                target = $(options.target);
            } else {
                target = options.target;
            }
            target.children().remove();
            var selectedIt = null;
            if (typeof options.promptMsg !== 'undefined') {
                if (typeof options.promptValue !== 'undefined') {
                    selectedIt = $("<option  value='" + options.promptValue + "'>" + options.promptMsg + "</option>").appendTo(target);
                } else {
                    selectedIt = $("<option  value=''>" + options.promptMsg + "</option>").appendTo(target);
                }
            } else {
                selectedIt = target;
            }
            if ($.isArray(options.data)) {
                $.each(options.data, function (i, o) {
                    var v = o[options.idField];
                    var isSelected = false;
                    if ($.isArray(options.selected)) {
                        $.each(options.selected, function (j, item) {
                            if (v === item) {
                                isSelected = true;
                            }
                        });
                    }
                    var it = null;
                    if (isSelected) {
                        it = $("<option value='" + v + "' selected='selected'>" + o[options.textField] + "</option>").appendTo(target);
                    } else {
                        it = $("<option value='" + v + "' >" + o[options.textField] + "</option>").appendTo(target);
                    }
                    it.data("data", o);
                    if (typeof options.defaultVal === 'string' || typeof options.defaultVal === 'number') {
                        if (options.defaultVal === v) {
                            selectedIt = it;
                        }
                    }
                });
            }
            selectedIt.attr('selected', 'selected');
            if (typeof options.onchange === 'function') {
                target.on('change', function () {
                    var opt = $(this).children("option:selected");
                    options.onchange($(opt));
                });
            }
        },
        /**
         * des加密 依赖于encrypt-min.js
         * ***/
        encryptData: function (message, key) {
            if (typeof window["CryptoJS"] === "undefined") {
                return message;
            }
            if (!key) {
                key = window["SRCUUID"];
            }
            var keyHex = window["CryptoJS"].enc.Utf8.parse(key);
            var encrypted = window["CryptoJS"].DES.encrypt(message, keyHex, {
                mode: window["CryptoJS"].mode.ECB,
                padding: window["CryptoJS"].pad.Pkcs7
            });
            return encrypted.toString();
        },
        /**
         * des解密  依赖于encrypt-min.js
         * ***/
        decryptData: function (message, key) {
            if (typeof window["CryptoJS"] === "undefined") {
                return message;
            }
            if (!key) {
                key = window["SRCUUID"];
            }
            var keyHex = window["CryptoJS"].enc.Utf8.parse(key);
            var decrypted = window["CryptoJS"].DES.decrypt({
                ciphertext: window["CryptoJS"].enc.Base64.parse(message)
            }, keyHex, {
                    mode: window["CryptoJS"].mode.ECB,
                    padding: window["CryptoJS"].pad.Pkcs7
                });
            return decrypted.toString(window["CryptoJS"].enc.Utf8);
        }
    });
    $B.bindTextClear = function ($form) {
        var fn = function () {
            var $text = $(this);
            if (!$text.hasClass("k_combox_input")) {
                $text.on("mouseenter.textbox", TextEvents.mouseover);
                $text.on("input.textbox", TextEvents.input);
                $text.on("mouseleave.textbox", TextEvents.mouseout);
            }
        };
        $form.find("input[type=text]").each(fn);
        $form.find("input[type=password]").each(fn);
    };
    //往所有输入框加上清空按钮    
    $(function () {
        setTimeout(function () {
            $B.bindTextClear($("body"));
        }, 1000);
    });
    window["$B"] = $B;
    return $B;
}));